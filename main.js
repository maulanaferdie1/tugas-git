// ini adalah tugas rumus bangunan ruang

function volumeTabung(jari2, tinggi) {
  if (tinggi % 7 == 0) {
    return (22 / 7) * jari2 ** 2 * tinggi;
  } else {
    return 3.14 * jari2 ** 2 * tinggi;
  }
}

function volumePrismaSegitiga(panjang, lebar, tinggi) {
  let volume;

  volume = panjang * lebar * tinggi;
  volume = 0.5 * volume;

  return volume;
}

let bola = (r) => {
  let phi = 3.14;
  let volume = (4 / 3) * phi * r ** 3;
  return volume;
};

function hitungVolumeLimas(a, t) {
  //hitung luas alas segitiga
  const luasAlas = 0.5 * a * t;
  //hitung volume limas
  const volumeLimas = (1 / 3) * luasAlas * t;
  return volumeLimas;
}

//function rumus kerucut rumus 

function hitungKerucut(r, t) {
    let volume = (1/3) * Math.PI * r * r * t;
    let garisPelukis = Math.sqrt(r * r + t * t);
    let luasPermukaan = Math.PI * r * (r + garisPelukis);
    return {
      volume: volume.toFixed(2),
      luasPermukaan: luasPermukaan.toFixed(2)
    };
  }

  console.log(hitungKerucut(4, 6));
